/* (c) https://github.com/MontiCore/monticore */

export * from "./monaco-diagnostics-contribution";
export * from "./monaco-outline-contribution";
