/* (c) https://github.com/MontiCore/monticore */

import { ContainerModule } from "inversify";
import { WorkerConnectionProvider } from "../../browser/messaging/connection";

export default new ContainerModule(bind => {
    bind(WorkerConnectionProvider).toSelf().inSingletonScope();
});
