/* (c) https://github.com/MontiCore/monticore */

export * from "./uri";
export * from "./application-protocol";
