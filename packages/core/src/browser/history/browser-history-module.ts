/* (c) https://github.com/MontiCore/monticore */

import { ContainerModule } from "inversify";
import { HistoryService, DefaultHistoryService } from "./history-service";

export default new ContainerModule(bind => {
    bind(HistoryService).to(DefaultHistoryService).inSingletonScope();
});
