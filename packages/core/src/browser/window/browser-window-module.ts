/* (c) https://github.com/MontiCore/monticore */

import { ContainerModule } from "inversify";
import { WindowService } from "@theia/core/lib/browser/window/window-service";
import { DefaultExtendedWindowService, ExtendedWindowService } from "./window-service";

export default new ContainerModule((bind, unbind, isBound, rebind) => {
    bind(ExtendedWindowService).to(DefaultExtendedWindowService).inSingletonScope();

    rebind(WindowService).to(DefaultExtendedWindowService).inSingletonScope();
});
