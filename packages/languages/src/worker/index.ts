/* (c) https://github.com/MontiCore/monticore */

export * from "./language-worker";
export * from "./language-worker-contribution";
