/* (c) https://github.com/MontiCore/monticore */

export * from "./browser-workspace";
export * from "./browser-workspace-contribution";
