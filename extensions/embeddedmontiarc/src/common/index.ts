/* (c) https://github.com/MontiCore/monticore */

export const EMBEDDEDMONTIARC_LANGUAGE_ID = "embeddedmontiarc";
export const EMBEDDEDMONTIARC_LANGUAGE_NAME = "EmbeddedMontiArc";
