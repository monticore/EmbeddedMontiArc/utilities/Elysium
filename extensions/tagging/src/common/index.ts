/* (c) https://github.com/MontiCore/monticore */

export const TAGGING_LANGUAGE_ID = "tagging";
export const TAGGING_LANGUAGE_NAME = "Tagging";
