/* (c) https://github.com/MontiCore/monticore */

import { ContainerModule } from "inversify";
import { LanguageWorkerContribution } from "@elysium/languages/lib/worker";
import { MontiCoreLanguageWorkerContribution } from "./monticore-contribution";

export default new ContainerModule(bind => {
    bind(LanguageWorkerContribution).to(MontiCoreLanguageWorkerContribution).inSingletonScope();
});
