/* (c) https://github.com/MontiCore/monticore */

export const MONTICORE_LANGUAGE_ID = "monticore";
export const MONTICORE_LANGUAGE_NAME = "MontiCore";
