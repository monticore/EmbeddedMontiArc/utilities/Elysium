/* (c) https://github.com/MontiCore/monticore */

export const STREAMUNITS_LANGUAGE_ID = "streamunits";
export const STREAMUNITS_LANGUAGE_NAME = "StreamUnits";
