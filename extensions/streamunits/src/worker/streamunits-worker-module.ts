/* (c) https://github.com/MontiCore/monticore */

import { ContainerModule } from "inversify";
import { LanguageWorkerContribution } from "@elysium/languages/lib/worker";
import { StreamUnitsLanguageWorkerContribution } from "./streamunits-contribution";

export default new ContainerModule(bind => {
    bind(LanguageWorkerContribution).to(StreamUnitsLanguageWorkerContribution).inSingletonScope();
});
