/* (c) https://github.com/MontiCore/monticore */

import { ContainerModule } from "inversify";
import { LanguageWorkerContribution } from "@elysium/languages/lib/worker";
import { EmbeddedMontiArcMathLanguageWorkerContribution } from "./embeddedmontiarcmath-contribution";

export default new ContainerModule(bind => {
    bind(LanguageWorkerContribution).to(EmbeddedMontiArcMathLanguageWorkerContribution).inSingletonScope();
});
