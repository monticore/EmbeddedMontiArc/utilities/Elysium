/* (c) https://github.com/MontiCore/monticore */

export const EMBEDDEDMONTIARCMATH_LANGUAGE_ID = "embeddedmontiarcmath";
export const EMBEDDEDMONTIARCMATH_LANGUAGE_NAME = "EmbeddedMontiArcMath";
