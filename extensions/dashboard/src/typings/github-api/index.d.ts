/* (c) https://github.com/MontiCore/monticore */

// TODO: Expand to real type definition.

declare module "github-api" {
    // tslint:disable-next-line:no-any
    const GitHub: any;
    export = GitHub;
}
