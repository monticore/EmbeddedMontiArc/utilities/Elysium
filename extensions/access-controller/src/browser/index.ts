/* (c) https://github.com/MontiCore/monticore */

export * from "./access-contributions";
export * from "./access-controller";
