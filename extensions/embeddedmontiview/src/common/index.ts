/* (c) https://github.com/MontiCore/monticore */

export const EMBEDDEDMONTIVIEW_LANGUAGE_ID = "embeddedmontiview";
export const EMBEDDEDMONTIVIEW_LANGUAGE_NAME = "EmbeddedMontiView";
