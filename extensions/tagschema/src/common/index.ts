/* (c) https://github.com/MontiCore/monticore */

export const TAGSCHEMA_LANGUAGE_ID = "tagschema";
export const TAGSCHEMA_LANGUAGE_NAME = "TagSchema";
