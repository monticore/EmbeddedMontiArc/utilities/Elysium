/* (c) https://github.com/MontiCore/monticore */

export const EMBEDDEDMONTIARCDL_LANGUAGE_ID = "embeddedmontiarcdl";
export const EMBEDDEDMONTIARCDL_LANGUAGE_NAME = "EmbeddedMontiArcDL";
