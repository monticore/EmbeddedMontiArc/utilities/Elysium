<!-- (c) https://github.com/MontiCore/monticore -->
# OCL
<p align="center">
    <img src="https://img.shields.io/badge/Extension_Version-0.1.2-blue.svg?longCache=true&style=flat-square"/>
    <img src="https://img.shields.io/badge/Grammar_Version-1.10.0--SNAPSHOT-blue.svg?longCache=true&style=flat-square"/>
</p>
<p align="center">
    <img src="doc/images/ocl.png"/>
</p>

## Description
To be written.

## References
To be added.

<!-- ## Application Programming Interface
The API documentation for this module can be found
[here](https://embeddedmontiarc.github.io/Elysium/plugins/ocl/docs). -->
