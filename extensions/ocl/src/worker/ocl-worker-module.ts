/* (c) https://github.com/MontiCore/monticore */

import { ContainerModule } from "inversify";
import { LanguageWorkerContribution } from "@elysium/languages/lib/worker";
import { OCLLanguageWorkerContribution } from "./ocl-contribution";

export default new ContainerModule(bind => {
    bind(LanguageWorkerContribution).to(OCLLanguageWorkerContribution).inSingletonScope();
});
