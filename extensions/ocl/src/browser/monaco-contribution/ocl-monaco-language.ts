/* (c) https://github.com/MontiCore/monticore */

/// <reference types="@theia/monaco/src/typings/monaco" />

import { DEFAULT_LANGUAGE_CONFIGURATION, DEFAULT_MONARCH_LANGUAGE } from "@elysium/languages/lib/browser";

export const configuration: monaco.languages.LanguageConfiguration = DEFAULT_LANGUAGE_CONFIGURATION;

export const monarchLanguage: monaco.languages.IMonarchLanguage = Object.assign({}, DEFAULT_MONARCH_LANGUAGE, {
    tokenPostfix: ".ocl",
    keywords: [
        "union", "intersect", "intersection", "and", "or", "xor", "void", "boolean", "byte", "short",
        "int", "long", "char", "float", "double", "extends", "import", "isin", "in", "public", "private",
        "protected", "final", "abstract", "local", "derived", "readonly", "static", "instanceof", "typeif",
        "then", "else", "if", "implies", "forall", "exists", "any", "let", "iterate", "isnew", "defined",
        "package", "context", "inv", "pre", "post", "new", "throws", "@pre", "(c)", "max", "min", "sum"
    ],
    operators: [
        "null", "true", "false"
    ],
    tokenizer: Object.assign({}, DEFAULT_MONARCH_LANGUAGE.tokenizer, {
        root: (() => {
            const tokenizer = DEFAULT_MONARCH_LANGUAGE.tokenizer;
            const root = tokenizer.root.slice(0);

            root.unshift([/\.\s*(max|min|sum)/, "meta"]);
            root.unshift([/(max|min|sum)\s*\./, "meta"]);

            return root;
        })()
    })
});
