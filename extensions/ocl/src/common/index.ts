/* (c) https://github.com/MontiCore/monticore */

export const OCL_LANGUAGE_ID = "ocl";
export const OCL_LANGUAGE_NAME = "OCL";
