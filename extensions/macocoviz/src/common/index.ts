/* (c) https://github.com/MontiCore/monticore */

export const MACOCOVIZ_LANGUAGE_ID = "macocoviz";
export const MACOCOVIZ_LANGUAGE_NAME = "MaCoCoVIZ";
