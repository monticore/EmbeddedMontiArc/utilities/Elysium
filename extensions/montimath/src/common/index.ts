/* (c) https://github.com/MontiCore/monticore */

export const MONTIMATH_LANGUAGE_ID = "montimath";
export const MONTIMATH_LANGUAGE_NAME = "MontiMath";
