<!-- (c) https://github.com/MontiCore/monticore -->
# Elysian Fields
<p align="center">
    <img src="https://img.shields.io/badge/Last_Updated-2019--07--24-blue.svg?longCache=true&style=flat-square"/>
</p>

## Core Team
<h4>
    <blockquote>2019.07.24: Elysium will no longer be maintained.</blockquote>
</h4>

## Former Contributors
| ![Michael von Wenckstern](https://github.com/vonwenckstern.png?size=150) | ![Jean-Marc Ronck](https://github.com/0xjmr.png?size=150) |
| :------------: | :-------------: |
| | |

<!--
## Contributors
We want __You__ for Software Development!
-->
